package controller;


import java.util.ArrayList;

import model.data_structures.Stack;
import model.logic.Expresiones;

public class Controller {

	private static Expresiones model = new Expresiones();

	public static boolean expresionBienFormada(String exp)
	{
		return model.expresionBienFormada(exp);
	}
	public static Stack <Character> ordenarPila()
	{
		return model.ordenarPila();
	}
	public static Stack <Character> crearStack(ArrayList <Character> x)
	{
		return new Stack<Character>(x);
	}
	public static String crearString(ArrayList <Character>x)
	{
		String nuevo="";
		for (int i = 0; i < x.size(); i++) {
			nuevo+=x.get(i).charValue();
		}
		return nuevo;
	}
}
