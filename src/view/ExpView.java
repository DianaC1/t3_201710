package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.Stack;

public class ExpView{


	private static void printMenu(){
		System.out.println("1. Crea una expresi�n");
		System.out.println("2. Verificar si expresi�n est� bien formada");
		System.out.println("3. Ordenar expresi�n");
		System.out.println("4. Salir");

	}
	private static ArrayList<Character> readData(){
		ArrayList<Character> data = new ArrayList<Character>();

		System.out.println("Digita una expresi�n, luego presiona enter: ");
		try{

			String linea = new Scanner(System.in).nextLine();

			char[] values = linea.toCharArray();
			for(int i=0;i< values.length;i++){
				data.add(values[i]);
			}
		}catch(Exception ex){
			System.out.println("Digita la expresi�n");
		}
		return data;
	}

	public static void main(String[] args)
	{
		boolean fin=false;
		String exp=null;
		Stack list=null;
		Scanner sc = new Scanner(System.in);
		while(! fin)
		{
			printMenu();

			int option = sc.nextInt();
			switch(option){
			case 1: 
				exp=Controller.crearString(readData());
				list=Controller.crearStack(readData());
				System.out.println("Se cre� la expresi�n");
				break;
			case 2:
				String expre=list.toString();
				System.out.println(Controller.expresionBienFormada(expre));
				break;

			case 3:System.out.println(Controller.ordenarPila());
			break;

			case 4: System.out.println("Adios!!  \n---------"); sc.close(); return;	

			default: System.out.println("--------- \n Invalid option !! \n---------");
			}
		}
	}
	
}


