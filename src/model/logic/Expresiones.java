package model.logic;


import model.data_structures.Queue;
import model.data_structures.Stack;

public class Expresiones {
	static final char CUADRADO_ABIERTO='[';
	static final char CUADRADO_CERRADO=']';
	static final char REDONDO_ABIERTO='(';
	static final char REDONDO_CERRADO=')';
	Stack <Character> stack;

	public boolean expresionBienFormada (String exp)
	{
		boolean si=false;
		
		
			if(corchetesAbiertos(exp,stack))
			{
				if( esNumeroPosk(exp,2,stack))
				{
					if(esOperadorPosk(exp,3,stack))
					{
						if(esNumeroPosk(exp,4,stack))
						{
							if(corchetesCerrados(exp,stack))
							{
							si=true;
							}
						}
					}
				}
				else if(esNumeroPosk(exp, 0,stack))
				{
					if(esOperadorPosk(exp,1,stack))
					{
						if(esNumeroPosk(exp,2,stack))
						{
							si=true;		
						}
					}
				}
			}
		return si;
	}
	public Stack<Character> ordenarPila()
	{ 
		int contCuadradoAbierto=0;
		int contCuadradoCerrado=0;
		int contRedondoAbierto=0;
		int contRedondoCerrado=0;
		int contUno=0;
		int contDos=0;
		int contTres=0;
		int contCuatro=0;
		int contCinco=0;
		int contSeis=0;
		int contSiete=0;
		int contOcho=0;
		int contNueve=0;
		int contSuma=0;
		int contResta=0;
		int contMult=0;
		int contDivi=0;
		Queue<Character>temp=new Queue<Character>();
		if(stack!=null)
		{
		for(int i=0;i< stack.tamanio();i++)
		{
			char actual=stack.pop();
			if(actual==CUADRADO_ABIERTO)
			{
				contCuadradoAbierto++;
			}
			else if(actual==CUADRADO_CERRADO)
			{
				contCuadradoCerrado++;
			}
			else if(actual==REDONDO_ABIERTO)
			{
				contRedondoAbierto++;
			}
			else if(actual==REDONDO_CERRADO)
			{
				contRedondoCerrado++;
			}
			else if(actual==1)
			{
				contUno++;
			}
			else if(actual==2)
			{
				contDos++;
			}
			else if(actual==3)
			{
				contTres++;
			}
			else if(actual==4)
			{
				contCuatro++;
			}
			else if(actual==5)
			{
				contCinco++;
			}
			else if(actual==6)
			{
				contSeis++;
			}
			else if(actual==7)
			{
				contSiete++;
			}
			else if(actual==8)
			{
				contOcho++;
			}
			else if(actual==9)
			{
				contNueve++;
			}
			else if(actual=='+')
			{
				contSuma++;
			}
			else if(actual=='-')
			{
				contResta++;
			}
			else if(actual=='*')
			{
				contMult++;
			}
			else if (actual=='/')
			{
				contDivi++;
			}	
		}

		if(contCuadradoAbierto>0)
		{
			for (int i = 0; i <contCuadradoAbierto; i++) 
			{
				temp.enqueue(CUADRADO_ABIERTO);
			}
		}
		if(contCuadradoCerrado>0)
		{
			for (int i = 0; i <contCuadradoCerrado; i++) 
			{
				temp.enqueue(CUADRADO_CERRADO);
			}
		}
		if(contRedondoAbierto>0)
		{
			for (int i = 0; i <contRedondoAbierto; i++) 
			{
				temp.enqueue(REDONDO_ABIERTO);
			}
		}
		if(contRedondoCerrado>0)
		{
			for (int i = 0; i <contRedondoCerrado; i++) 
			{
				temp.enqueue(REDONDO_CERRADO);
			}
		}
		if(contSuma>0)
		{
			for (int i = 0; i < contSuma; i++) {
				temp.enqueue('+');
			}
		}
		if(contResta>0)
		{
			for (int i = 0; i < contResta; i++) {
				temp.enqueue('-');
			}
		}
		if(contMult>0)
		{
			for (int i = 0; i < contMult; i++) {
				temp.enqueue('*');
			}
		}
		if(contDivi>0)
		{
			for (int i = 0; i < contDivi; i++) {
				temp.enqueue('/');
			}
		}
		if(contUno>0)
		{
			for (int i = 0; i <contUno; i++) {
				temp.enqueue('1');
			}
		}
		if(contDos>0)
		{
			for (int i = 0; i <contDos; i++) {
				temp.enqueue('2');
			}
		}
		if(contTres>0)
		{
			for (int i = 0; i <contTres; i++) {
				temp.enqueue('3');
			}
		}
		if(contCuatro>0)
		{
			for (int i = 0; i <contCuatro; i++) {
				temp.enqueue('4');
			}
		}
		if(contCinco>0)
		{
			for (int i = 0; i <contCinco; i++) {
				temp.enqueue('5');
			}
		}
		
		if(contSeis>0)
		{
			for (int i = 0; i <contSeis; i++) {
				temp.enqueue('6');
			}
		}
		if(contSiete>0)
		{
			for (int i = 0; i <contSiete; i++) {
				temp.enqueue('7');
			}
		}
		if(contOcho>0)
		{
			for (int i = 0; i <contOcho; i++) {
				temp.enqueue('8');
			}
		}
		if(contNueve>0)
		{
			for (int i = 0; i <contNueve; i++) {
				temp.enqueue('9');
			}
		}
		stack.push(temp.dequeue());
		}
		return stack;
	}
    public boolean corchetesAbiertos(String expresion,Stack <Character> stack)
    {
    	boolean si=false;
    	char[] arreglo=expresion.toCharArray();
    	for(int i=0;i<arreglo.length;i++)
    	{
    		char actual=arreglo[i];
    		if(actual==CUADRADO_ABIERTO||actual==REDONDO_ABIERTO)
    		{
    			si=true;stack.push(actual);
    		}
    	}
    	return si;
    }
    public boolean corchetesCerrados(String expresion,Stack <Character>stack)
    {
    	boolean si=false;
    	char[] arreglo=expresion.toCharArray();
    	for(int i=0;i<arreglo.length;i++)
    	{
    		char actual=arreglo[i];
    		char ultimo=arreglo[arreglo.length-1];
    		if(actual==CUADRADO_ABIERTO && ultimo==CUADRADO_CERRADO)
    		{
    			si=true;
    			stack.push(actual);
    		}
    		else if(actual==REDONDO_ABIERTO && ultimo==REDONDO_CERRADO)
    		{
    			si=true;
    			stack.push(actual);
    		}
    	}
    	return si;
    }
	public boolean verificacionCorchetes(String expresion)
	{
		boolean bien=false;
		char[] arreglo=expresion.toCharArray();
		int cont1=0;
		int cont2=0;
		int cont3=0;
		int cont4=0;
		for(int i=0;i<arreglo.length;i++)
		{
			if(arreglo[i]==CUADRADO_ABIERTO)
			{
				cont1++;
			}
			if(arreglo[i]==CUADRADO_CERRADO)
			{
				cont2++;
			}
			if(arreglo[i]==REDONDO_ABIERTO)
			{
				cont3++;
			}
			if(arreglo[i]==REDONDO_CERRADO)
			{
				cont4++;
			}
		}
		if(cont1==cont2)
		{
			if(cont3==cont4)
			{
				bien=true;
			}
		}


		return bien;

	}
	public boolean esNumeroPosk(String exp,int pos,Stack <Character>stack)
	{
		boolean si=false;
		char[] arreglo=exp.toCharArray();
		if(pos<arreglo.length)
		{
			char actual=arreglo[pos];
			if(actual>=0 && actual<=9)
			{
				si=true;
				stack.push(actual);
			}
		}
		return si;
	}
	public boolean corchete(String exp)
	{
		boolean si=false;
		char[] arreglo=exp.toCharArray();
		for (int i = 0; i < arreglo.length; i++) {
             char actual=arreglo[i];
			if(actual==CUADRADO_ABIERTO || actual==CUADRADO_CERRADO || actual==REDONDO_ABIERTO || actual==REDONDO_CERRADO)
			{
				si=true;
			}
		}
		return si;
	}
	
	public boolean esOperadorPosk(String exp, int pos,Stack <Character>stack)
	{
		boolean si=false;
		char[] arreglo=exp.toCharArray();
			char actual=arreglo[pos];
			if(actual=='+'|| actual=='-'||actual=='*'|| actual=='/')
			{
				si=true;
				stack.push(actual);
			}
		return si;
	}
	
}

