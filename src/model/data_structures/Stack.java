package model.data_structures;

import java.util.ArrayList;

public class Stack <T> implements IStack <T>
{
	private ListaEncadenada<T> lista;

	public Stack()
	{
		lista=new ListaEncadenada <T>();
	}
	
	public Stack(ArrayList< Character>list )
	{
		lista=new ListaEncadenada<T>();
		
		if(list!=null)
		{
			for(Character values:list)
			{
				lista.agregarElementoFinal((T) values);
			}
		}
		lista.toString();
		
	}
	
	public void push(T item) {
    lista.agregarElementoInicio(item);
	}
	public T getItemInicio()
	{
		T nuevo=lista.darElemento(0);
		return nuevo;
	}
	public T pop()
	{
		return lista.darElemento(0);
	}
	public boolean isEmpty()
	{
		boolean esta=false;
		if(lista.darElemento(0)==null)
		{
			esta=true;
		}
		return esta;
	}
	public int tamanio()
	{
		return lista.darNumeroElementos();
	}
	public T darElementoPosicionK(int pos)
	{
		T elemento=lista.darElemento(pos);
		
		return elemento;
	}

}
