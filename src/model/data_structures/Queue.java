package model.data_structures;

public class Queue <T> implements IQueue <T>{
	private ListaEncadenada <T> lista;
	public Queue()
	{
		lista=new ListaEncadenada <>();
	}
	public T getItemInicio()
	{
		T nuevo=lista.darElemento(0);
		return nuevo;
	}
	public void enqueue(T item)
	{
		lista.agregarElementoFinal(item);
	}
	public T dequeue()
	{
	 T eliminado=lista.eliminarElementoInicio();
	 return eliminado;
	 
	}

	public boolean isEmpty()
	{
		boolean esta=false;
		if(lista.darElemento(0)==null)
		{
			esta=true;
		}
		return esta;
	}

	public int tamanio()
	{
		return lista.darNumeroElementos();
	}
}
