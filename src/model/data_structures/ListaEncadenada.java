package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo <T> primero;
	private int tamanio;

	public ListaEncadenada()
	{
		primero=new NodoSencillo<T>(null,null);
	}

	private class ListaIterator<T> implements Iterator<T>
	{
		private NodoSencillo <T> actual;
		ListaIterator<T> nueva=new ListaIterator<T>();
		@Override
		public boolean hasNext() {
			boolean tiene=false;
			if(nueva.hasNext())
			{
				tiene=true;
			}
			return tiene;
		}
		public T next() {
			NodoSencillo <T> temp=actual;
			actual=actual.getSiguiente();
			return temp.getElemento();
		}
	}

	public Iterator<T> iterator() {
		return new ListaIterator<T>();
	}

	@Override
	public void agregarElementoFinal(T elem) {

		NodoSencillo<T> actual=primero;
		NodoSencillo <T> nuevoNodo=new NodoSencillo<T>(elem,null);
		nuevoNodo.setElemento(elem);
		if(primero==null)
		{
			primero=nuevoNodo;
			tamanio++;
			return;
		}
		while(actual.getSiguiente()!=null)
		{
			actual=actual.getSiguiente();
		}
		actual.setSiguiente(nuevoNodo);
		tamanio++;
	}
	public T eliminarElementoInicio()
	{
		if(primero == null)
		{
			System.out.println("La lista se encuentra vac�a");
		}
		T item = primero.getElemento();
		NodoSencillo<T> siguientePrimero = primero.getSiguiente();
		primero.setElemento(null);
		primero = siguientePrimero;
		tamanio--;
		return item;
	}
	
	public void agregarElementoInicio(T elem) {
	NodoSencillo <T> nuevoNodo=new NodoSencillo<T>(elem,null);
	nuevoNodo.setElemento(elem);
	if(primero==null)
	{
		primero=nuevoNodo;
		tamanio++;
		return;
	}
	else{
		nuevoNodo.setSiguiente(primero);
		primero=nuevoNodo;
	}
	}
	
	
	public T darElemento(int pos) {
		NodoSencillo <T> actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getSiguiente();
			contador++;
		}
		return(T) actual.getElemento();
	}

	public int darNumeroElementos() {
		return tamanio;
	}

	public T darElementoPosicionActual() {
		NodoSencillo <T> actual=primero;
		T elem=null;
		if(actual.getElemento()!=null)
		{
			elem=actual.getElemento();
		}
		return elem;
	}

	public boolean avanzarSiguientePosicion() {
		boolean avanza=false;
		if(primero.getSiguiente()!=null)
		{
			primero=primero.getSiguiente();
		}
		return avanza;
	}

	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		return false;
	}

}
