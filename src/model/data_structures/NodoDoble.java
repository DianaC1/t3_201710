package model.data_structures;

import model.data_structures.NodoDoble;

public class NodoDoble<T> {
	
	private T item;
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	
	public NodoDoble(){
		
		item = null;
		siguiente = null;
	}
	
	public void cambiarItem(T item){
		
		this.item = item;
	}
	
	public T darItem(){
		
		return this.item;
	}
	
	public void cambiarSiguiente(NodoDoble<T> siguiente){
		
		this.siguiente = siguiente;
	}
	
	public NodoDoble<T> darSiguiente(){
		
		return this.siguiente;
	}
	
	public void cambiarAnterior(NodoDoble<T> anterior){
		
		this.anterior = anterior;
	}
	
	public NodoDoble<T> darAnterior(){
		
		return this.anterior;
	}

}
