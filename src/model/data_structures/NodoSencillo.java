package model.data_structures;

public class NodoSencillo <T>
{
	private T elemento;

	private NodoSencillo <T> siguiente;

	public NodoSencillo(T elemento, NodoSencillo <T> siguiente){
		this.elemento=elemento;
		this.siguiente=siguiente;
	}
	
	public void setSiguiente(NodoSencillo<T> siguiente )
	{
		this.siguiente=siguiente;
	}

	public NodoSencillo <T> getSiguiente()
	{
		return siguiente;
	}

	public void setElemento(T elemento)
	{
		this.elemento=elemento;
	}

	public T getElemento()
	{
		return elemento;
	}
}
