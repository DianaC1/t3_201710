package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	
	private NodoDoble<T> primero;
	private int tamanio;
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> actual = primero;
		NodoDoble<T> nuevo = new NodoDoble<>();
		nuevo.cambiarItem(elem);
		
		if(actual == null){
			
			primero = nuevo;
			tamanio++;
			return;
		}
		
		while(actual.darSiguiente() != null){
			actual = actual.darSiguiente();
		}
		actual.cambiarSiguiente(nuevo);
		nuevo.cambiarAnterior(actual);
		tamanio++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		
		NodoDoble<T> actual = primero;
		NodoDoble<T> nodo = null;
		int contador = 0;
		
		if(pos == 0){
			
			nodo = actual;
			return nodo.darItem();
		}
		
		while(actual.darSiguiente() != null){
			
			contador++;
			if(contador == pos){
				
				nodo = actual;
			}
			actual = actual.darSiguiente();
		}
		
		return nodo.darItem();
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tamanio;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return primero.darItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		
		if(primero.darSiguiente() != null){
			
			return true;
		}
		
		return false;
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		
		if(primero.darAnterior() != null){
			
			return true;
		}
		return false;
	}

}
