import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase 
{
	Queue <String> nueva=new Queue<>();

	public void setup1()
	{
		nueva.enqueue("a");
		nueva.enqueue ("b");
		nueva.enqueue("c");
	}
	public void enqueueTest()
	{
		setup1();
		nueva.enqueue("d");
		assertEquals("d", nueva.getItemInicio());
	}
	public void dequeueTest()
	{
		setup1();
		nueva.dequeue();
		assertEquals("c", nueva.dequeue());
	}
	public void tamanioTest()
	{
		setup1();
		assertEquals(3,nueva.tamanio());
	}
	public void isEmptyTest()
	{
		setup1();
		assertEquals(false, nueva.isEmpty());
	}
}
