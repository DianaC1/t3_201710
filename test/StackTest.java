import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase 
{
	Stack <String> nueva=new Stack<>();

	public void setup1()
	{
		nueva.push("a");
		nueva.push ("b");
		nueva.push("c");
	}
	public void pushTest()
	{
		setup1();
		nueva.push("d");
		assertEquals("d", nueva.getItemInicio());
	}
	public void popTest()
	{
		setup1();
		assertEquals("c", nueva.pop());
	}
	public void tamanioTest()
	{
		setup1();
		assertEquals(3,nueva.tamanio());
	}
	public void isEmptyTest()
	{
		setup1();
		assertEquals(false, nueva.isEmpty());
	}
}
